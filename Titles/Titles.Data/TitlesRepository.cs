﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Titles.Model;
namespace Titles.Data
{
    public class TitlesRepository
    {
        TitlesEntities context = new TitlesEntities();

        public TitlesRepository()
        {
          
        }
        public List<Title> GetTitles()
        {
            using (var context = new TitlesEntities())
            {
                {
                    context.Configuration.ProxyCreationEnabled = false;

                    var query = (from x in context.Titles
                                 select x).ToList();
                    return query;
                }

            }
        }

        public Title GetTitleDetails(int id)
        {

              using(var context=new TitlesEntities())
              {
                  //context.Configuration.ProxyCreationEnabled = false;
                  var  query=(from x in context.Titles
                                   where x.TitleId==id
                                select x).SingleOrDefault();
                  return query;                
              }

        }

    }
}
