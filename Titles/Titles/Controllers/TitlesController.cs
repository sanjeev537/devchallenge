﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Titles.Model;
using Titles.Data;

namespace Titles.Controllers
{
    public class TitlesController : ApiController
    {
        TitlesRepository repo = new TitlesRepository();
        // GET api/<controller>
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET api/<controller>/5
        [Route("api/titles")]
        public List<Title> Get()
        {
           
            var ret=repo.GetTitles();
            return ret;
        }
        [Route("api/titles/{id}/details")]
        public Title GetDetails(int id)
        {
            var ret = repo.GetTitleDetails(id);
            return ret;
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}