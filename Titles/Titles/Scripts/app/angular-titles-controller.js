﻿

app.controller('titlesController', function ($scope, $resource, $uibModal, $log, $timeout) {


    $resource('api/titles').query().$promise.then(function (titles) {
        $scope.titles = titles;
    });
    $scope.mydata = "test";
    
    $scope.opentest = function () {
        alert("test");
    };
    $scope.open = function (title) {
       
        var $uibModalInstance = $uibModal.open({
            templateUrl: 'myModalContent.html',
            controller: 'modalInstanceController',
            resolve: {
                mydata: function () {
                    return title;
                }
            }
           });
                   

        $uibModalInstance.opened.then(function () {
           // $scope.loadData($uibModalInstance);
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
    $scope.loadData = function (aModalInstance) {
        $log.info("starts loading");
        //$resource('api/titles').query().$promise.then(function (titles) {
        //    $scope.titles = titles;
        //});
        $timeout(function() {
            $log.info("data loaded");
            aModalInstance.setMyData({ TitleName: "data loaded..." });
        }, 3000);
    };

  
    

});

app.controller('modalInstanceController', function ($scope, $resource,$uibModalInstance,mydata) {

    $scope.mydata = mydata;

    $uibModalInstance.setMyData = function (theData) {
        $scope.mydata = theData;
    };

    $scope.ok = function () {
        $uibModalInstance.close('close');
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});